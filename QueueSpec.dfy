datatype Element<T(==)> = Element(value:T, weight:int);

class PriorityQueue<T(==)>
{
  var N: int;  // capacity
  var n: int;  // current size
  ghost var Repr: set<object>;

  var a: array<Element<T>>;  // private implementation of PriorityQueue
  var m: map<T,int>;         // map from element to index
  var Contents : set<T>;
  var weight : map<T,int>;

  predicate mostlyValid()
  reads this,Repr;
  {
    this in Repr && a in Repr &&
    a != null && a.Length == N+1 &&
    0 <= n && n <= N &&
    (forall t :: t in m <==> t in Contents) &&
    (forall i :: 1 <= i <= n ==> a[i].value in Contents) &&
    (forall i :: 1 <= i <= n ==> m[a[i].value] == i) &&
    (forall i :: 1 <= i <= n ==> weight[a[i].value] == a[i].weight) &&
    (forall t :: t in Contents ==> 1 <= m[t] <= n) &&
    (forall i,j :: i in m && j in m && i != j ==> m[i] != m[j])

  }

  predicate valid()
  reads this,Repr;
  {
    mostlyValid() &&
    (forall j :: 2 <= j && j <= n ==> a[j/2].weight <= a[j].weight) //&&
  }


  constructor(N: int) 
  requires N >= 0;
  modifies this;
  ensures fresh(Repr - {this});
  ensures valid();
  ensures n == 0;
  ensures this.N == N;

  method insert(t: T, k: int)
  requires valid();
  requires n < N;
  requires t !in Contents;
  modifies this,Repr;
  ensures a == old(a);
  ensures fresh(Repr - old(Repr));
  ensures valid(); 
  ensures Contents == {t} + old(Contents);
  ensures n == old(n) +1 && N == old(N);
  ensures (set i | 1 <= i <= n :: a[i]) == 
          (set i | 1 <= i < n :: old(a[i])) + {Element(t,k)};

  function method min() : T
  requires valid() && 1 <= n;
  reads this,Repr;
  ensures min() in Contents;
  {
    a[1].value
  }

  method deleteMin() 
  requires valid();
  requires n >= 1;
  modifies this,Repr;
  ensures valid();
  ensures n == old(n) - 1;
  ensures Contents == old(Contents) - {old(min())};
  ensures fresh(Repr - old(Repr));

  method decreaseKey(t: T, k: int)
  requires valid();
  requires t in Contents;
  modifies this,Repr;
  ensures fresh(Repr - old(Repr));
  ensures Contents == old(Contents);
  ensures n == old(n);
  ensures N == old(N);
  ensures valid(); 

  function method isEmpty() : bool
  requires valid();
  reads this,Repr;
  {
    n == 0
  }
}

lemma Least(q : PriorityQueue)
  requires q != null && q.valid();
  ensures (forall t :: t in q.Contents ==> q.a[q.m[t]].weight >= q.a[q.m[q.min()]].weight);
  {
  assert (forall j :: 2 <= j <= q.n ==> q.a[j/2].weight <= q.a[j].weight);

  assert q.n >= 2 ==> q.a[1].weight <= q.a[2].weight;

  var i := 2;
  assert (q.n >= 2 ==> q.a[1].weight <= q.a[2].weight);
  assert (q.n >= 2 ==> (forall j :: 2 <= j <= 2 ==> q.a[1].weight <= q.a[j].weight));
  while (i < q.n)
  invariant (q.n >= 2 ==> i <= q.n);
  invariant (q.n >= 2 ==> (forall j :: 2 <= j <= i ==> q.a[1].weight <= q.a[j].weight));
  {
    assert (i + 1)/2 < i;
    i := i + 1;
  }

  assert (forall i :: 2 <= i <= q.n ==> q.a[1].weight <= q.a[i].weight);
  assert (forall t :: t in q.Contents ==> q.a[q.m[t]].weight >= q.a[1].weight);
  assert (forall t :: t in q.Contents ==> q.a[q.m[t]].weight >= q.a[q.m[q.min()]].weight);
  }
