class Graph {
  predicate method hasVertex(v : int) 
    reads this;
  {
    0 <= v < size
  }

  predicate Valid() 
    reads this;
  {
    |neighbors| == size &&
    (forall s :: s in neighbors[..] ==> |s| <= size) &&
    (forall i :: hasVertex(i) ==> (forall j :: j in neighbors[i] ==> 0 <= j < size)) //&&
  }

  predicate hasPath(prev: array<int>, src: int, dst: int, l : int)
    requires prev != null;
    reads this,prev;
    decreases prev.Length - l;
  {
    prev != null && 0 <= src < prev.Length && 0 <= dst < prev.Length && 
    prev.Length - l > 0 &&
    ( prev[dst] == src || 
      hasPath(prev,src,prev[dst], l + 1)
    )
  }

  var neighbors: seq<seq<int>>;
  var size: nat;
  
}

class Dijkstra {
  
    method shortestPaths(G: Graph, source: int) returns (prev: array<int>)
        requires G != null;
        requires G.Valid();
        requires G.hasVertex(source);
    {
      var dist := new int[G.size];
      prev := new int[G.size];
      var q := new PriorityQueue<int>(G.size);
      assert fresh(q);
      assert prev !in q.Repr;

      var i := 0;
      while (i < G.size)
      invariant q.valid();
      invariant q.N - q.n == G.size - i;
      invariant q.Contents == set j | 0 <= j < i :: j;
      invariant fresh({q} + q.Repr); 
      invariant prev !in q.Repr;
      invariant (forall i :: i in q.Contents ==> 0 <= i < G.size);
      invariant (forall j :: 0 <= j < i ==> 
            if j == source then prev[j] == source else prev[j] == -1);
      {
        if i == source
        {
          dist[i] := 0;
          prev[i] := source;
        }
        else {
          dist[i] := 99999;
          prev[i] := -1;
        }
        assert (forall j :: 0 <= j <= i ==> 
              if j == source then prev[j] == source else prev[j] == -1);
        q.insert(i,dist[i]);
        i := i + 1;
      }

      assert q.Contents == set i | 0 <= i < G.size :: i;

      assert (forall i :: 0 <= i < G.size ==> 
        prev[i] == -1 || G.hasPath(prev,source,i,0));

      while !q.isEmpty()
      decreases q.n;
      invariant q.valid();
      invariant fresh(q.Repr);
      invariant q.Contents <= set i | 0 <= i < G.size :: i;
      {
        var u := q.min();
        q.deleteMin();

        var neighbors := G.neighbors[u];
        var k := |neighbors| - 1;
        ghost var old_qn := q.n;
        while (k > 0)
        invariant q.n == old_qn;
        invariant q.valid();
        invariant fresh(q.Repr);
        invariant q.Contents <= set i | 0 <= i < G.size :: i;
        {
          var v := neighbors[k];
          var alt := dist[u] + 1;
          if v in q.Contents && alt < dist[v] {
            dist[v] := alt;
            prev[v] := u;
            q.decreaseKey(v,alt);
          }
          k := k -1;
        }
      }
    }
}
