datatype Element<T(==)> = Element(value:T, weight:int);

class PriorityQueue<T(==)>
{
  var N: int;  // capacity
  var n: int;  // current size
  ghost var Repr: set<object>;

  var a: array<Element<T>>;  // private implementation of PriorityQueue
  var m: map<T,int>;         // map from element to index
  var Contents : set<T>;

  predicate mostlyValid()
  reads this,Repr;
  {
    this in Repr && a in Repr &&
    a != null && a.Length == N+1 &&
    0 <= n && n <= N &&
    (forall t :: t in m <==> t in Contents) &&
    (forall i :: 1 <= i <= n ==> a[i].value in Contents) &&
    (forall i :: 1 <= i <= n ==> m[a[i].value] == i) &&
    (forall t :: t in Contents ==> 1 <= m[t] <= n) &&
    (forall i,j :: i in m && j in m && i != j ==> m[i] != m[j])
  }

  predicate valid()
  reads this,Repr;
  {
    mostlyValid() &&
    (forall j :: 2 <= j && j <= n ==> a[j/2].weight <= a[j].weight)
  }

  lemma {:verify true} Least()
    requires this != null && this.valid();
    ensures (forall t :: t in Contents ==> a[1].weight <= a[m[t]].weight);
    {
    assert (forall j :: 2 <= j <= n ==> a[j/2].weight <= a[j].weight);

    assert n >= 2 ==> a[1].weight <= a[2].weight;

    var i := 2;
    assert (n >= 2 ==> a[1].weight <= a[2].weight);
    assert (n >= 2 ==> (forall j :: 2 <= j <= 2 ==> a[1].weight <= a[j].weight));
    while (i < n)
    invariant (n >= 2 ==> i <= n);
    invariant (n >= 2 ==> (forall j :: 2 <= j <= i ==> a[1].weight <= a[j].weight));
    {
      assert (i + 1)/2 < i;
      i := i + 1;
    }

    assert (forall i :: 2 <= i <= n ==> a[1].weight <= a[i].weight);
    assert (forall t :: t in Contents ==> a[m[t]].weight >= a[1].weight);
    }


  constructor {:verify true} (N: int) 
  requires N >= 0;
  modifies this;
  ensures fresh(Repr - {this});
  ensures valid();
  ensures n == 0;
  ensures this.N == N;
  {
    this.N := N;
    a := new Element[N+1];
    n := 0;
    m := map[];
    Repr := {this};
    Repr := Repr;
    Repr := Repr + {a};
    Contents := {};
  }

  method {:verify true} insert(t: T, k: int)
  requires valid();
  requires n < N;
  requires t !in Contents;
  modifies this,Repr;
  ensures a == old(a);
  ensures fresh(Repr - old(Repr));
  ensures valid(); 
  ensures Contents == {t} + old(Contents);
  ensures n == old(n) +1 && N == old(N);
  ensures (set i | 1 <= i <= n :: a[i]) == 
          (set i | 1 <= i < n :: old(a[i])) + {Element(t,k)};
  {
    n := n + 1;
    assert (forall i :: 1 <= i <= n-1 ==> m[a[i].value] == i);
    a[n] := Element(t,k);
    assert a[n].value == t;
    m := m[a[n].value := n];
    assert m[t] == n;
    assert m[a[n].value] == n;
    assert(set i | 1 <= i <= n :: a[i]) == 
            (set i | 1 <= i < n :: old(a[i])) + {Element(t,k)};
    Contents := Contents + {t};
    assert a[m[t]].weight == k;
    siftUp(n);
  }
  method {:verify true} siftUp(k: int)
  requires mostlyValid();
  requires 1 <= k <= n;
  requires (forall j :: 2 <= j <= n && j != k ==> a[j/2].weight <= a[j].weight);
  requires (forall j :: 1 <= j/2/2 && j/2 == k && j <= n ==> a[j/2/2].weight <= a[j].weight);
  modifies this, Repr;
  ensures fresh(Repr - old(Repr));
  ensures a == old(a);
  ensures valid();
  ensures Contents == old(Contents);
  ensures n == old(n);
  ensures (set i | 1 <= i <= n :: a[i]) == (set i | 1 <= i <= n :: old(a[i]));
  {
    ghost var old_n_set := set i | 1 <= i <= n :: a[i];
    var i := k;
    while (i > 1)
    invariant Repr == old(Repr);
    invariant a == old(a);
    invariant n == old(n);
    invariant Contents == old(Contents);
    invariant i <= n;
    invariant mostlyValid();
    invariant (forall j :: 2 <= j <= n && j != i ==> a[j/2].weight <= a[j].weight);
    invariant (forall j :: 1 <= j/2/2 && j/2 == i && j <= n ==> a[j/2/2].weight <= a[j].weight);
    invariant (set i | 1 <= i <= n :: a[i]) == old_n_set;
    {
      if (a[i/2].weight < a[i].weight) {
        return;
      }
      m:= m[a[i].value := i/2][a[i/2].value := i];
      a[i/2], a[i] := a[i], a[i/2];
      i := i / 2;
      assert (forall j :: 2 <= j <= n && j != i ==> a[j/2].weight <= a[j].weight);
    } 
  }

  function method {:verify true} min() : T
  requires valid() && 1 <= n;
  reads this,Repr;
  ensures min() in Contents;
  {
    a[1].value
  }

  method {:verify true} deleteMin() 
  requires valid();
  requires n >= 1;
  modifies this,Repr;
  ensures valid();
  ensures Contents == old(Contents) - {old(min())};
  ensures n == old(n) - 1;
  ensures fresh(Repr - old(Repr));
  ensures (set i | 1 <= i <= n :: a[i]) 
        == (set i | 1 <= i <= n+1 :: old(a[i])) - {old(a[1])};
  {
    var removed_val := a[1].value;
    a[1] := a[n];
    Contents := Contents - {removed_val};
    m := map t | t in m && t != removed_val :: m[t];
    m := m[a[1].value := 1];
    n := n-1;
    if n == 0 {
      m := map[];
      Contents := {};
    }
    siftDown(1);
  }
  
  method {:verify true} siftDown(k: int)
  requires 1 <= k;
  requires mostlyValid();
  requires (forall j :: 2 <= j && j <= n && j/2 != k ==> a[j/2].weight <= a[j].weight);
  requires (forall j :: 2 <= j && j <= n && 1 <= j/2/2 && j/2/2 != k ==> a[j/2/2].weight <= a[j].weight);
  modifies this,Repr;
  ensures fresh(Repr - old(Repr));
  ensures valid();
  ensures n == old(n);
  ensures N == old(N);
  ensures Contents == old(Contents);
  ensures (set i | 1 <= i <= n :: a[i]) == (set i | 1 <= i <= n :: old(a[i]));
  {
    ghost var old_n_set := set i | 1 <= i <= n :: a[i];
    var i := k;
    while (2*i <= n)  // while i is not a leaf
    invariant 1 <= i && mostlyValid();
    invariant n == old(n) && N == old(N) && Contents == old(Contents);
    invariant (forall j :: 2 <= j && j <= n && j/2 != i ==> a[j/2].weight <= a[j].weight);
    invariant (forall j :: 2 <= j && j <= n && 1 <= j/2/2 && j/2/2 != i ==> a[j/2/2].weight <= a[j].weight);
    invariant (Repr == old(Repr));
    invariant (set i | 1 <= i <= n :: a[i]) == old_n_set;
    {
      var smallestChild;
      if (2*i + 1 <= n && a[2*i + 1].weight < a[2*i].weight) {
        smallestChild := 2*i + 1;
      } else {
        smallestChild := 2*i;
      }
      if (a[i].weight <= a[smallestChild].weight) {
        return;
      }
      m := m[a[smallestChild].value := i][a[i].value := smallestChild];
      a[smallestChild], a[i] := a[i], a[smallestChild];
      i := smallestChild;
      assert 1 <= i/2/2 ==> a[i/2/2].weight <= a[i].weight;
    }
  }

  method {:verify true} decreaseKey(t: T, k: int)
  requires valid();
  requires t in Contents;
  modifies this,Repr;
  ensures fresh(Repr - old(Repr));
  ensures Contents == old(Contents);
  ensures n == old(n);
  ensures N == old(N);
  ensures valid();
  ensures (set i | 1 <= i <= n :: a[i]) 
          == (set i | 1 <= i <= n :: old(a[i])) 
              - {old(a[m[t]])}
              + {Element(t,k)};
  {
    var index := m[t];
    assume k <= a[index].weight;
    a[index] := Element(a[index].value, k);
    assert mostlyValid();
    assert (set i | 1 <= i <= n :: a[i]) 
            == (set i | 1 <= i <= n :: old(a[i])) 
                - {old(a[m[t]])}
                + {Element(t,k)};
    siftUp(index);
  }

  function method {:verify true} isEmpty() : bool
  requires valid();
  reads this,Repr;
  ensures isEmpty() <==> n == 0;
  {
    n == 0
  }
}
